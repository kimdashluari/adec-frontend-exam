import $ from 'jquery';
import { scrollTo } from './utilities';

const $scrollTos = $('.js.scroll-to');

const handleScrollToClick = e => {
  e.preventDefault();
  const targetSelector = $(e.currentTarget).data('target');
  scrollTo(targetSelector);
};

const init = () => {
  $scrollTos.on('click', handleScrollToClick);
};

// Init
$(init);