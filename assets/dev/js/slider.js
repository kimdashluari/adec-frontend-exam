import $ from 'jquery';
import '~node_modules/slick-carousel/slick/slick.js';

const $sliders = $('.js.slider');
const $prevButton = `
  <button class="slider__prev">
    <i class="fas fa-arrow-left"></i>
  </button>
`;
const $nextButton = `
  <button class="slider__next">
    <i class="fas fa-arrow-right"></i>
  </button>
`;

const init = () => {
  $sliders.each((i, slider) => {
    const $slider = $(slider);
    let settings = {
      prevArrow: $prevButton,
      nextArrow: $nextButton
    };

    $slider.slick(settings);
  });
};

$(init);