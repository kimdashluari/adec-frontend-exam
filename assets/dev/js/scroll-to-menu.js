import $ from 'jquery';
import { debounce, scrollTo } from './utilities';

const $scrollToMenu = $('.js.scroll-to-menu');
const $window = $(window);
const scrollThreshold = 320;

const toggleScrollToMenuButton = () => {
  const shouldDisplay = $window.scrollTop() >= scrollThreshold;

  $scrollToMenu.toggleClass('is-active', shouldDisplay);
};

$window.on('scroll', debounce(toggleScrollToMenuButton));
$scrollToMenu.on('click', e => scrollTo());