import $ from 'jquery';
import { debounce } from './utilities';

const $window = $(window);
const $heightEqualizers = $('.js.height-equalizer');
let instances = [];

class HeightEqualizer {
  constructor($root) {
    this.$root = $root;
    this.$items = $('.js.height-equalizer__item', this.$root);
    this.groups = this.$root.data('height-groups').split(',');
  }

  init() {
    this.applyHeights();
  }

  applyHeights() {
    this.$items.removeAttr('style');
    this.groups.forEach(group => {
      const $items = this.$items.filter(`[data-height-group="${group}"]`);
      const highestHeight = this.getHighestHeight($items);
      $items.height(highestHeight);
    });
  }

  getHighestHeight($items) {
    const itemHeights = $items.map((i, elem) => $(elem).outerHeight()).toArray();
    return Math.max(...itemHeights);
  }
}

// Init
const init = () => {
  $heightEqualizers.each((i, elem) => {
    const heightEqualizer = new HeightEqualizer($(elem));
    heightEqualizer.init();
    // Save instances for reuse
    instances.push(heightEqualizer);
  });
};

const reInitHeightEqualizers = () => {
  instances.forEach(instance => instance.init());
};

// Init
$(init);

// Resize
$window.on('resize', debounce(reInitHeightEqualizers));
