import $ from 'jquery';
import mapMarkerPin from '../images/map-marker-pin.png';
import { debounce } from './utilities';

const $googleMap = $('#google-map');

if (!$googleMap.length) return;

const $window = $(window);
const API_KEY = 'AIzaSyC9AX5zd7S2DiuFS1PLvqS3ywdgZkGf5Q0';
const coordinates = {
  lat: 38.872341,
  lng: -77.265823,
};

// Build google map script tag
const buildScript = () => {
  $('<script>').attr({
    src: `https://maps.googleapis.com/maps/api/js?key=${API_KEY}&callback=initMap`,
    async: true
  }).appendTo(
    $('head')
  );
};

// @NOTE: Stolen and revamped from Avada WP Theme
const buildMapStyles = (map, data = {}) => {
  const stylers = [];
  let style = [];
  let mapType;
  let style_color = '';
  
  if(data.hue != '') {
    stylers.push({ hue: data.hue });
  }

  if(data.saturation != '') {
    stylers.push({ saturation: data.saturation });
  }
  
  if(stylers.length)
  {
    style = [
      {
        featureType: 'all',
        elementType: 'all',
        stylers: stylers
      }, {
        featureType: 'poi',
        stylers: [
        { visibility: 'off' }
        ]
      }
    ];
    
    if(data.saturation == 'fill')
    {
      style_color = data.hue || '#242424';
      
      const c = style_color.substring(1);      // strip #
      const rgb = parseInt(c, 16);   // convert rrggbb to decimal
      const r = (rgb >> 16) & 0xff;  // extract red
      const g = (rgb >>  8) & 0xff;  // extract green
      const b = (rgb >>  0) & 0xff;  // extract blue
      
      let luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709
      let lightness = 1;
      let street_light = 2;
      
      if (luma > 60) {
        lightness = -1;
        street_light = 3;
      }
      if (luma > 220) {
        lightness = -2;
        street_light = -2;
      }
      
      style = [
        {'featureType':'all','elementType':'all','stylers':[{'color':style_color},{'lightness':0}]},
        {'featureType':'all','elementType':'labels.text.fill','stylers':[{'color':style_color},{'lightness':(25 * street_light)}]},
        {'featureType':'all','elementType':'labels.text.stroke','stylers':[{'visibility':'on'},{'color':style_color},{'lightness':3}]},
        {'featureType':'all','elementType':'labels.icon','stylers':[{'visibility':'off'}]},
        {'featureType':'administrative','elementType':'geometry.fill','stylers':[{'color':style_color},{'lightness':30}]},
        {'featureType':'administrative','elementType':'geometry.stroke','stylers':[{'color':style_color},{'lightness':30},{'weight':1.2}]},
        {'featureType':'landscape','elementType':'geometry','stylers':[{visibility: 'simplified'},{'color':style_color},{'lightness':3}]},
        {'featureType':'poi','elementType':'geometry','stylers':[{ 'visibility': 'off' }]},
        {'featureType':'road.highway','elementType':'geometry.fill','stylers':[{'color':style_color},{'lightness':-3}]},
        {'featureType':'road.highway','elementType':'geometry.stroke','stylers':[{'color':style_color},{'lightness':2},{'weight':0.2}]},
        {'featureType':'road.arterial','elementType':'geometry','stylers':[{'color':style_color},{'lightness':-3}]},
        {'featureType':'road.local','elementType':'geometry','stylers':[{'color':style_color},{'lightness':-3}]},
        {'featureType':'transit','elementType':'geometry','stylers':[{'color':style_color},{'lightness':-3}]},
        {'featureType':'water','elementType':'geometry','stylers':[{'color':style_color},{'lightness':-20}]}
      ];
    }	
    
    mapType = new google.maps.StyledMapType(style, { name:'adec_map_style' });
    map.mapTypes.set('adec_styled_map', mapType);
    map.setMapTypeId('adec_styled_map');
  }
};

// Initialize Google Map
const initMap = () => {
  const gMaps = google.maps;

  const map = new gMaps.Map($googleMap.get(0), {
    center: coordinates,
    disableDefaultUI: true,
    zoom: 14,
  });

  const marker = new gMaps.Marker({
    position: coordinates,
    icon: mapMarkerPin,
    map,
  });

  // Apply color styles
  buildMapStyles(map, {
    hue: '#1b1b1b',
    saturation: 'fill'
  });

  // Reposition map to make the marker pin
  // visible to the left side of the contact section
  positionMap(map);
};

// Position map by panning
const positionMap = (map) => {
  map.panBy(0, 0);

  if ($window.width() > 1024) {
    let timeoutId = setTimeout(() => {
      map.panBy(400, 0);

      clearTimeout(timeoutId);
    });
  }
};

const onWindowResize = () => {
  initMap();
};

// Make `initMap` to be global
window.initMap = initMap;

$(buildScript);
$window.on('resize', debounce(onWindowResize));