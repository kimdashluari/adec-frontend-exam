import $ from 'jquery';
import overlay from './overlay';

const $drawers = $('.js.drawer');

class Drawer {
  constructor($element) {
    this.$element = $element;
    this.id = this.$element.data('drawer-id');
    this.$opener = $(`.js.drawer-opener[data-drawer-id="${this.id}"]`);
    this.$closer = $(`.js.drawer-closer[data-drawer-id="${this.id}"]`);
  }

  init() {
    this.$opener.on('click', this.openDrawer.bind(this));
    this.$closer.on('click', this.closeDrawer.bind(this));
  }

  openDrawer() {
    this.$element.addClass('is-active');

    overlay.activate(() => {
      this.closeDrawer();
      overlay.deactivate();
    });
  }

  closeDrawer() {
    this.$element.removeClass('is-active');
    overlay.deactivate();
  }
}

const init = () => {
  $drawers.each((i, $drawer) => {
    const drawer = new Drawer($($drawer));
    drawer.init();
  });
};

$(init);