import $ from 'jquery';

const init = () => {
  const $heroPlayVideo = $('.js.hero-video-cta');
  const $heroPlayVideoIcon = $('.js.hero-video-cta__icon', $heroPlayVideo);
  const $heroPlayVideoText = $('.js.hero-video-cta__text', $heroPlayVideo);

  $heroPlayVideo.on('click', e => {
    e.preventDefault();
    $heroPlayVideo.toggleClass('is-playing');

    const isPlaying = $heroPlayVideo.hasClass('is-playing');
    $heroPlayVideoIcon.toggleClass('fa-play', !isPlaying);
    $heroPlayVideoIcon.toggleClass('fa-pause', isPlaying);

    const text = isPlaying ? 'Pause video'  : 'Play Video';
    $heroPlayVideoText.text(text);
  });
};

$(init);