import $ from 'jquery';
import { debounce, isInViewport, scrollTo } from './utilities';

const $window = $(window);

const itemClickHandler = ($items, event) => {
  event.preventDefault();
  const $element = $(event.currentTarget);
  const targetSelector = $element.data('target');
  scrollTo(targetSelector);

  $items.removeClass('is-active');
  $element.addClass('is-active');
};

const startSpying = $items => {
  $items.removeClass('is-active');

  $items.each((i, item) => {
    const $item = $(item);
    const targetSelector = $item.data('target')
    const $targetElement = $(targetSelector);
    const shouldActivate = isInViewport(
      $targetElement.get(0),
      50
    );

    if (shouldActivate) {
      $item.addClass('is-active');
    }
  });
};

const handleWindowScroll = ($items) => {
  return debounce(startSpying.bind(null, $items), 1);
};

$.fn.adecScrollSpy = function () {
  this.each((i, element) => {
    const $element = $(element);
    const $items = $('.js.scrollspy__item', $element);

    $items.on('click', itemClickHandler.bind(null, $items));

    $window.on('scroll', handleWindowScroll($items));
  });

  return this;
};

$('.js.scrollspy').adecScrollSpy();