import $ from 'jquery';

const $expander = $('.js.expander');

class Expander
{
  constructor($root) {
    this.$root = $root;
    this.$link = $('.js.expander__link', $root);
    this.$content = $('.js.expander__content', $root);
  }

  init() {
    this.$link.on('click', this.toggleContent.bind(this));
  }

  toggleContent(e) {
    e.preventDefault();
    this.$root.toggleClass('is-active');
  }
}

const init = () => {
  $expander.each((i, element) => {
    const expander = new Expander(
      $(element)
    );

    expander.init();
  });
};

$(init);