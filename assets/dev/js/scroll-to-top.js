import $ from 'jquery';
import { debounce, isInViewport, scrollTo } from './utilities';

const $scrollToTop = $('.js.scroll-to-top');
const $window = $(window);

const toggleScrollToTopButton = () => {
  const shouldDisplay = isInViewport(
    $('.js.footer').get(0),
    1
  );

  $scrollToTop.toggleClass('is-active', shouldDisplay);
};

$window.on('scroll', debounce(toggleScrollToTopButton));
$scrollToTop.on('click', e => scrollTo());