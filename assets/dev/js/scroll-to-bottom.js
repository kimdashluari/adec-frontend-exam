import $ from 'jquery';
import { debounce, isInViewport, scrollTo } from './utilities';

const $scrollToMenu = $('.js.scroll-to-bottom');
const $window = $(window);

const toggleScrollToMenuButton = () => {
  const shouldDisplay = !isInViewport(
    $('.js.footer').get(0),
    1
  );

  $scrollToMenu.toggleClass('is-active', shouldDisplay);
};

$window.on('scroll', debounce(toggleScrollToMenuButton));
$scrollToMenu.on('click', e => scrollTo());