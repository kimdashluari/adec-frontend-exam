import $ from 'jquery';

export const debounce = (callback, delay = 400) => {
  let inDebounce;

  return function(...args) {
    const context = this;
    clearTimeout(inDebounce);
    inDebounce = setTimeout(callback.bind(context, ...args), delay);
  };
};

export const scrollTo = (targetSelector = document.body, $elementToScroll = $('html, body')) => {
  const $targetElement = $(targetSelector);

  if (!$targetElement.length) return false;

  $elementToScroll.animate({
    scrollTop: $targetElement.offset().top
  });
};

export const isInViewport = (el, percentVisible) => {
  let
    rect = el.getBoundingClientRect(),
    windowHeight = (window.innerHeight || document.documentElement.clientHeight);

  return !(
    Math.floor(100 - (((rect.top >= 0 ? 0 : rect.top) / +-rect.height) * 100)) < percentVisible ||
    Math.floor(100 - ((rect.bottom - windowHeight) / rect.height) * 100) < percentVisible
  )
}