import $ from 'jquery';

const init = () => {
  const $button = $('.js.play-video');
  const targetVideo = $button.data('video');
  const $video = $(`.js.video[data-video="${targetVideo}"]`).get(0);
  let isPlaying = false;

  $button.on('click', e => {
    e.preventDefault();

    !isPlaying ? $video.play() : $video.pause();

    isPlaying = !isPlaying;
  });
};

$(init);