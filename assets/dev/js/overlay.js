import $ from 'jquery';

class Overlay {
  constructor() {
    this.$element = $('.js.overlay');
    this.isActive = false;
  }

  activate(clickCallback = () => {}) {
    if (this.isActive) return;

    this.$element.addClass('is-active');
    this.$element.on('click', clickCallback);
    this.isActive = true;
  }

  deactivate() {
    if (!this.isActive) return;

    this.$element.removeClass('is-active');

    this.$element.off('click');
    this.isActive = false;
  }
}

export default new Overlay();